### this repository contains 2 essential scripts for hard and soft bakcups of mysql, when it comes to remote copying. 

#### Structure :
Server1 : web-server (nginx and web-application)  
Server2 : database server (mysql-server and backup scripts)  
Server3 : backup server (where soft and hard backups are stored)   

[Diagram of Structure](/DiagramOverview.png)    

##### Step 1 [Database setup]   

To setup database on my second server we need following packages are installed : mariadb-server   

we can install our mariadb-server from its official website by simply following instruction. (I preffered repository installation)   
[mariadb-server installation website](https://mariadb.org/download/?t=repo-config)  

After installation we can further proceed to mysql_secure_installation  
```bash
mysql_secure_installation
```
After that we will create necessary tables and users according to our python app. I will create 1 users with 2 IP accesses. one for view and backup and other one will be used by our web-application.    
```bash
'dbuser'@'192.168.253.161';
+---------------------------------------------------------------------------------------------------------------------+
| Grants for dbuser@192.168.253.161                                                                                   |
+---------------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `dbuser`@`192.168.253.161` IDENTIFIED BY PASSWORD '*E5852165B7B2DDE63D99EB8299E79615CC8D193C' |
| GRANT ALL PRIVILEGES ON `people`.* TO `dbuser`@`192.168.253.161`                                                    |
+---------------------------------------------------------------------------------------------------------------------+

'dbuser'@'192.168.253.160';
+---------------------------------------------------------------------------------------------------------------------+
| Grants for dbuser@192.168.253.160                                                                                   |
+---------------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `dbuser`@`192.168.253.160` IDENTIFIED BY PASSWORD '*C734D43EFFE7989F30205448C8556EB5C4D942A9' |
| GRANT ALL PRIVILEGES ON `people`.* TO `dbuser`@`192.168.253.160`                                                    |
+---------------------------------------------------------------------------------------------------------------------+
```



I created one database (people) and one table (identity) with following columns :    
```bash
+------------------+
| Tables_in_people |
+------------------+
| identity         |
+------------------+

+---------+-------------+------+-----+---------+----------------+
| Field   | Type        | Null | Key | Default | Extra          |
+---------+-------------+------+-----+---------+----------------+
| ID      | int(11)     | NO   | PRI | NULL    | auto_increment |
| Name    | varchar(20) | NO   |     | NULL    |                |
| Surname | varchar(20) | NO   |     | NULL    |                |
+---------+-------------+------+-----+---------+----------------+
```
we need to add our server1 and server2 addresses to /etc/mysql/mariadb.conf.d/50-server.cnf    
```conf
# Instead of skip-networking the default is now to listen only on
# localhost which is more compatible and is not less secure.
bind-address            = 192.168.253.160,192.168.253.161
```

##### Step 2 [Application setup]

I used simple python flask application with an index.html. My web-application takes 2 inputs and with those inputs inserts a new record to dedicated database.    
   
Here are the reference files :   
[app.py](/WebApplication/app.py)   
[index.html](/WebApplication/index.html)   

application tree must be look like this :   
```bash 
/app
    /templates
        index.html
    app.py
```
To make application connect to database we need to establish ssh keybased passwordless authentication between server1 and server2   
```bash
ssh-keygen
ssh-copy-id
```

then we need to install required packages :    
python3 :
```bash
sudo apt install python3
python3 --version
```
Flask, pymysql and mariadb-client :   
```bash
pip install Flask
pip install pymysql
sudo apt-get install mariadb-client
```
Then we will need gunicorn to serve our application :    
```bash
pip install gunicorn
```

##### Step 3 [Nginx setup]   
I will do simple nginx forwarding. You can enhance it with https:   

```conf
server {
        listen 80 ;
        server_name 192.168.253.160;
        root /app ;

        index index.html;
        try_files $uri /index.html;

        location ~ /\.ht {
                deny all;
        }


        location / {
                proxy_pass http://192.168.253.160:8000;
        }
}
```

To serve the application (you must cd to where app.py stands) :    
```bash
gunicorn -b 0.0.0.0:8000 app:app
```

##### Step 4 [Backup setup]

You can check each script individually by clicking it :    
   
[soft backup script](/soft-backup.sh)   
[hard backup script](/hard-backup.sh)  
   
  
Generally soft backup script creates a backup .sql file using mariadb-dump utility. with scripting skills we are enhaced it with date addon to files for easy look up, variable-styled procedure for easy maintenance.     
   
Hard backup script does its job in old ways. Shutting down mariadb service , backups all folder Structure of mysql and restarts mariadb-server    
  
To make both script work we need to create environment for key based passwordless authentication between server2 and server3    
```bash 
ssh-keygen
ssh-copy-id
``` 
   
We will do hard backups by hand. So its simply going inside of the server and running the script.    
   
But for soft backups work repeteadly, we need to create cronjob.   

Here we have a script for doing this automatically and by our wanted interval. 
  
[Automatic crontab writer](crontab_writer.sh)   
