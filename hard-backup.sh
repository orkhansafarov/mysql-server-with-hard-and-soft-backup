#!/bin/bash

# # backup server credentials
host=192.168.253.162
user=user
backup_folder=/home/user/mysql_hard_backups  # full path
folder_name=db-hard-bckp_$(date +%d-%m-%y)

mysql_db_folder=/var/lib/mysql

remote_backup_folder=/home/user/mysql_hard_backups  # full path

        # SCRIPT


sudo -S systemctl stop mariadb

mkdir -p $backup_folder/${folder_name}

sudo -S cp -r $mysql_db_folder/* $backup_folder/${folder_name}/

sudo scp -r $backup_folder/${folder_name}  $user@$host:$remote_backup_folder

sudo -S systemctl start mariadb
