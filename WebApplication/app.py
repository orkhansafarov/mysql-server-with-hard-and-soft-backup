from flask import Flask, request, render_template
import pymysql

app = Flask(__name__)

# Database Configuration
db = pymysql.connect(
    host='192.168.253.161',  # MariaDB server IP
    user='dbuser',
    password='12345w',  # Replace with the actual password
    database='people',  # Database name
)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        # Get the name and surname from the form
        name = request.form.get('name')
        surname = request.form.get('surname')

        # Store the data in the database
        cursor = db.cursor()
        cursor.execute("INSERT INTO identity (Name, Surname) VALUES (%s, %s)", (name, surname))
        db.commit()
        cursor.close()

        return "Name and Surname have been stored in the database successfully!"

    return render_template('index.html')

if __name__ == '__main__':
    app.run(host='192.168.253.160', port=5000, debug=True)
