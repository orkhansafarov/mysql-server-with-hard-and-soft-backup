#!/bin/bash

home_dir=/home/user

crontab -l > $home_dir/crontab_temp

# identify when cronjob will be scheduled
minute=7
hour=2
day=1
month=3
weekday=*

# identify script or command
script_path=$home_dir/soft-backup.sh
command="bash $script_path"


echo "$minute $hour $day $month $weekday $command" >> $home_dir/crontab_temp

crontab $home_dir/crontab_temp

rm -rf $home_dir/crontab_temp
