#!/bin/bash


        # VARIABLE DEFINITION
# folder name with full path (must end with "/")
backup_folder=/home/user/db_backups/

# file name (personal preference)
file_name=dbuser_backup
file_name_date=$file_name_$(date +%d-%m-%y_%T)

# which file will script read the mysql user credentials and database name  (full path)
default_file=/home/user/credentials
db_name=people

# backup server credentials
host=192.168.253.162
user=user
remote_backup_folder=/home/user/mysql_soft_backups  # full path

        # SCRIPT
mariadb-dump --defaults-file=$default_file $db_name > $backup_folder${file_name_date}.sql
echo "${file_name_date}.sql is created under $backup_folder"
echo "sending to backup server ..."
scp $backup_folder${file_name_date}.sql $user@$host:$remote_backup_folder

